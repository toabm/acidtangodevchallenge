import { CommonRoutesConfig } from '../common/common.routes.config';
import ChuckNorrisController from './controllers/chucknorris.controller';
import express from 'express';

// The body() method will validate fields and generate an errors list�stored in the express.
import { body } from 'express-validator';
// We then need our own middleware to check and make use of the errors list.
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';


export class ChucknorrisRoutesConfigRoutes extends CommonRoutesConfig {

    apiPath: string = '';

    constructor(app: express.Application) {
        super(app, 'ChuckNorrisRoutes');
    }

    configureRoutes(): express.Application {

        // Set API module fot ChuckNorris routes.
        this.apiPath =  `${this.baseURL}/chucknorris`;

        // The API's URL http://localhost:3000/api/v1 will just inform that the API Server is up and running.
        this.app
            .route(`${this.baseURL}`).get(CommonRoutesConfig.apiWorking);

        // http://localhost:3000/api/v1/chucknorris/random --> Get a random joke
        this.app
            .route(`${this.apiPath}/random`)
            .get(ChuckNorrisController.getRandom)

        // http://localhost:3000/api/v1/chucknorris/categories --> Get a list of categories.
        this.app
            .route(`${this.apiPath}/categories`)
            .get(ChuckNorrisController.getCategories)

        // http://localhost:3000/api/v1/chucknorris/search --> Get jokes by free text search.
        this.app
            .route(`${this.apiPath}/search`)
            .get(ChuckNorrisController.getByText)

        return this.app;
    }
}

