export interface ChucknorrisInterface {
    getRandomByCategory: (category: string) => Promise<any>;
    getRandom: () => Promise<any>;
    getByText: (text: string) => Promise<string>;
    getCategories:  () => Promise<string>;
}