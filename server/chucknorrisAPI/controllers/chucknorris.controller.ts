/**
 * The idea behind controllers is to separate the route configuration from the code that finally processes a route request.
 *
 * That means that all validations should be done before our request reaches the controller. The controller only needs to
 * know what to do with the actual request because if the request made it that far, then we know it turned out to be valid.
 * The controller will then call the respective service of each request that it will be handling.
 */

// We import express to add types to the request/response objects from our controller functions
import express from 'express';
// We import our newly created user services
import chucknorrisService from '../services/chucknorris.service';


// We use debug with a custom context
import debug from 'debug';

const log: debug.IDebugger = debug('app:chucknorris-controller');

class ChucknorrisController {

    /**
     *
     */
    constructor() {
    }

    /**
     * Get random joke.
     *
     * The request may include a query poaram "category" to search for a random joke by category.
     * @param req
     * @param res
     */
    async getRandom(req: express.Request, res: express.Response) {
        const joke = req.query.hasOwnProperty("category")
            ? await chucknorrisService.getRandomByCategory(`${req.query.category}`) // Get joke by category
            : await chucknorrisService.getRandom(); // Get random joke
        res.status(200).send(joke);
    }


    /**
     * Get list of categories
     * @param req
     * @param res
     */
    async getCategories(req: express.Request, res: express.Response) {
        const categories = await chucknorrisService.getCategories();
        res.status(200).send(categories);
    }


    /**
     * Get joke by free text search
     * @param req
     * @param res
     */
    async getByText(req: express.Request, res: express.Response) {
        const jokes = await chucknorrisService.getByText(`${req.query.query}`);
        res.status(200).send(jokes);
    }
}

export default new ChucknorrisController();