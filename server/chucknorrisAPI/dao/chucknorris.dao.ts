// Promise based HTTP client for the browser and node.js
import axios from "axios";
// Import connection to MongoDB.
import debug from 'debug';

// Import DTOs.
import { ChuckNorrisDto }    from '../dto/chucknorris.dto';

// We will feed data from external API:
const BASE_URL = `https://api.chucknorris.io/jokes`;

const log: debug.IDebugger = debug('app:chucknorris-dao');

class ChuckNorrisDao {



    /**
     * Class constructor
     */
    constructor() {
        log('Created new instance of ChuckNorrisDao');
    }


    // GET A RANDOM CHUCK NORRIS FACT.
    async getRandom(limit = 25, page = 0) {
        return axios.get(`${BASE_URL}/random`).then( (data) => {return data.data});
    }

    // GET LIST OF CATEGORIES
    async getCategories() {
        return axios.get(`${BASE_URL}/categories`).then( (data) => {return data.data});
    }

    // GET A RANDOM CHUCK NORRIS FACT BY CATEGORY
    async getRandomByCategory(category:string) {
        return axios.get(`${BASE_URL}/random?category=${category}`).then( (data) => {return data.data});
    }

    // GET ARRAY OF JOKES BY FREE SEARCH TEXT
    async getByText (text: string, limit = 25, page = 0) {
        return axios.get(`${BASE_URL}/search?query=${text}`).then( (data) => {return data.data});
    }



}


// Using the singleton pattern, this class will always provide the same instance
export default new ChuckNorrisDao();