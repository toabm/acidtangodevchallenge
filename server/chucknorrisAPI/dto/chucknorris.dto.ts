/**
 * DTO contains just the fields that we want to pass between the API client and our database.
 */
export interface ChuckNorrisDto {
    id: string;
    icon_url: string;
    url: string;
    value?: string;
}