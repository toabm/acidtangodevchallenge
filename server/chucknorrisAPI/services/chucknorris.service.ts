import ChuckNorrisDao from '../dao/chucknorris.dao';
import { ChucknorrisInterface } from '../interfaces/chucknorris.interface';
import { ChuckNorrisDto } from '../dto/chucknorris.dto';



/**
 * Users Service class
 *
 * We�re using updateUserById() for both PUT and PATCH
 */
class ChuckNorrisService implements ChucknorrisInterface {


    /**
     *  Passing limit and page parameters along to getUsers() for pagination purposes.
     * @param limit
     * @param page
     */
    // async list(limit: number, page: number) {
    //     return ChuckNorrisDao.getRandom();
    // }




    // Retrieve list of categories from API Server
    getCategories(): Promise<string> {return ChuckNorrisDao.getCategories()}
    // Get a random Chuck Norris Joke from API Server
    getRandom(): Promise<any> {return ChuckNorrisDao.getRandom()}
    // Get random joke by category.
    getRandomByCategory(category: string): Promise<any> {return ChuckNorrisDao.getRandomByCategory(category)}
    // Retrieve list of categories from API Server
    getByText(text: string): Promise<any> {return ChuckNorrisDao.getByText(text)}

}
export default new ChuckNorrisService();