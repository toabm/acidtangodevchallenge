/**
 * We�ll use express-validator, which is quite stable, easy to use, and decently documented.
 *
 * While we could use the validation functionality that comes with Mongoose, express-validator provides extra features.
 * For example, it comes with an out-of-the-box validator for email addresses, which in Mongoose would require us to
 * code a custom validator.
 */
import express from 'express';
import { validationResult } from 'express-validator';

class BodyValidationMiddleware {
    verifyBodyFieldsErrors(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).send({ errors: errors.array() });
        }
        next();
    }
}

export default new BodyValidationMiddleware();