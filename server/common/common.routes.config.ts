import express from 'express';
import chucknorrisService from "../chucknorrisAPI/services/chucknorris.service";
export abstract class CommonRoutesConfig {
    app: express.Application;
    name: string;
    baseURL: string;

    protected constructor(app: express.Application, name: string) {
        this.app = app;
        this.name = name;
        this.baseURL = '/api/v1';
        this.configureRoutes();
    }
    getName() {return this.name;}

    // This forces any class extending CommonRoutesConfig to provide an implementation matching that signature�if it
    // doesn�t, the TypeScript compiler will throw an error.
    abstract configureRoutes(): express.Application


    /**
     * Get random joke.
     *
     * The request may include a query poaram "category" to search for a random joke by category.
     * @param req
     * @param res
     */
    static async apiWorking(req: express.Request, res: express.Response) {
        // this is a simple route to make sure everything is working properly
        res.status(200).send(`API Server working smoothly...:)`)
    }
}