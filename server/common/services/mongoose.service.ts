/**
 * Mongoose is an object data modeling (ODM) library that will simplify communications with MongoDB
 */
import mongoose from 'mongoose';
import debug from 'debug';

const log: debug.IDebugger = debug('app:mongoose-service');

class MongooseService {
    private count = 0;
    private mongooseOptions = {
        useNewUrlParser: true,          // Without this set to true, Mongoose prints a deprecation warning.
        useUnifiedTopology: true,       // The Mongoose documentation recommends setting this to true to use a newer connection management engine.
        serverSelectionTimeoutMS: 5000, // For the purpose of the UX of this demo project, a shorter time than the default of 30 seconds means that any readers who forget to start MongoDB before Node.js will see helpful feedback about it sooner, instead of an apparently unresponsive back end.
        useFindAndModify: false,        // This causes Mongoose to use a newer native MongoDB feature instead of an older Mongoose shim.
    };

    /**
     * Class constrtuctor.
     */
    constructor() {
        this.connectWithRetry();
    }

    getMongoose() {return mongoose;}

    /**
     * This method retries mongoose.connect() in case our application starts but the MongoDB service is not yet running.
     *
     * Since it�s in a singleton constructor, connectWithRetry() will only be run once, but it will retry the connect()
     * call indefinitely, with a pause of retrySeconds seconds whenever a timeout occurs.
     */
    connectWithRetry = () => {
        log('Attempting MongoDB connection (will retry if needed)');
        mongoose
            // Attempts to connect to our local MongoDB service (running with docker-compose) and will time out after
            // serverSelectionTimeoutMS milliseconds.
            //.connect('mongodb://mongo/searchs', this.mongooseOptions) // This URL is used to connect with MongoDB in the dockerized server (both the APIServer & MongoDB Service).
            .connect('mongodb://localhost:27017/api-db', this.mongooseOptions) // This URL is used to connect with MongoDB when we run the server by "npm start" and dockerize only the MongoDB service.
            .then(() => { log('MongoDB is connected')})
            .catch((err:any) => {
                const retrySeconds = 5;
                log(`MongoDB connection unsuccessful (will retry #${++this.count} after ${retrySeconds} seconds):`, err);
                setTimeout(this.connectWithRetry, retrySeconds * 1000);
            });
    };
}
export default new MongooseService();