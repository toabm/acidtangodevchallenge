// Import connection to MongoDB.
import mongooseService from '../../common/services/mongoose.service';
import shortid from "shortid";
import debug from 'debug';
// Import DTOs
import {CreateSearchDto} from "../dto/create.search.dto";




const log: debug.IDebugger = debug('app:searchs-dao');

class SearchsDao {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // USERS SCHEMA FOR MONGOOSE ///////////////////////////////////////////////////////////////////////////////////////
    // User Schema for Mongoose: The user schema defines which fields should exist in our MongoDB collection called Users,
    // while the DTO entities defines which fields to accept in an HTTP request.
    Schema = mongooseService.getMongoose().Schema;
    searchSchema = new this.Schema({
        _id: String,
        search: String,
        result: [],
        total: Number,
    }, { id: false }); // Mongoose models provide a virtual id getter by default, so we�ve disabled that option above with { id: false }

    Search = mongooseService.getMongoose().model('Search', this.searchSchema);
    // END OF USERS SCHEMA FOR MONGOOSE ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * Class constructor
     */
    constructor() {
        log('Created new instance of SearchsDAO');
    }


    // GET A RANDOM CHUCK NORRIS FACT.
    async getSearchs(limit = 25, page = 0) {
        return this.Search.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    // GET LIST OF CATEGORIES
    async saveSearch(searchsFields: CreateSearchDto) {
        const id = shortid.generate();
        const search = new this.Search({_id: id,...searchsFields});
        await search.save();
        return id;
    }

    // GET A RANDOM CHUCK NORRIS FACT BY CATEGORY
    async sendEmail() {
    }


}


// Using the singleton pattern, this class will always provide the same instance
export default new SearchsDao();