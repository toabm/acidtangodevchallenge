import SearchsDao from '../dao/searchs.dao';
import { searchsInterface} from '../interfaces/searchs.interface';




/**
 * Users Service class
 *
 * We�re using updateUserById() for both PUT and PATCH
 */
class SearchsService implements searchsInterface {




    async getSearchs(limit: number, page: number): Promise<any> {
        return SearchsDao.getSearchs()
    }

    saveSearch(resource: any): Promise<any> {return SearchsDao.saveSearch(resource);}

    sendEmail(resource: any): Promise<any> {return SearchsDao.sendEmail()}

}
export default new SearchsService();