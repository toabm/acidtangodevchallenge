/**
 * DTO contains just the fields that we want to pass between the API client and our database.
 */
export interface CreateSearchDto {
    // id: string; // Mongoose automatically makes an _id field available
    search: String,
    result: [],
    total: Number,
}