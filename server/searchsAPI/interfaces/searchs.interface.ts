export interface searchsInterface {
    getSearchs: (limit: number, page: number) => Promise<any>;
    saveSearch: (resource: any) => Promise<any>;
    sendEmail: (resource: any) => Promise<any>;
}