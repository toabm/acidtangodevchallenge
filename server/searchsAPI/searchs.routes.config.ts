import { CommonRoutesConfig } from '../common/common.routes.config';
import SearchsController from './controllers/searchs.controller';
import express from 'express';

// The body() method will validate fields and generate an errors list�stored in the express.
import { body } from 'express-validator';
// We then need our own middleware to check and make use of the errors list.
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';


// const routes: { } = {
//     random: '/random',
//     categories: '/categories'
// }


export class SearchsRoutesConfigRoutes extends CommonRoutesConfig {

    apiPath: string = '';

    constructor(app: express.Application) {
        super(app, 'SearchsRoutes');
    }

    configureRoutes(): express.Application {

        // Set API module fot ChuckNorris routes.
        this.apiPath =  `${this.baseURL}/searchs`;

        this.app
            .route(`${this.apiPath}`)
            .get(SearchsController.getSearchs)
            .post(SearchsController.saveSearch)

        this.app
            .route(`${this.apiPath}/email`)
            .post(SearchsController.sendEmail)


        return this.app;
    }
}

