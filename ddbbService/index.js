const express = require("express");
const database = require("./database.js");
const cors = require('cors');
const app = express();
const nodemailer = require('nodemailer');

// Use corrs
app.use(cors());

// Parse body as JSON objects.
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));

// Init mongo DB.
database.initializeMongo();


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'umboweti.auto@gmail.com',
    pass: 'dfegdfgdf98dfg7KJH8768lkiu__wd'
  },
  //auth: {
  //  type: 'OAuth',
  //  user: '823735424788-u4rimlkisgbmbitb0l1vj9n218b7qodg.apps.googleusercontent.com',
  //  accessToken: 'CRKBTZ9uHZ5QOAeB4bV5QNAF'
  //}
});


/**
 *
 */
function sendEmail(mailOptions) {
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Test endpoint
 */
app.get("/", function(req, res) {
  res.send("Hello World!")
});

/**
 * GET ALL SAVED SEARCHS
 */
app.get("/searchs", function (req, res) {


  database.Search.find(function (err, searchs) {
    if (err) return res.error(err);
    res.send(`<style>
        table, th, td {
            border: 1px solid black;
            text-align: center;
        }
        li {text-align: justify;}
        li:nth-child(2n) {background-color: lightcyan }
        </style><table style="border: 2px solid grey">
        <thead><tr style="border: 2px solid grey"><td>Búsqueda</td><td>Total</td><td>Resultados</td></tr></thead>
        <tbody>${searchs.map(r => `<tr style="border: 2px solid grey"><td>${r.search}</td><td>${r.total}</td>
                <td><ul>${r.result.map(entry => `<li>${JSON.stringify(entry)}</li>`).join("")}</ul></td>
            </tr>`).join("")}</tbody></table>`)
    //res.json(searchs);
  });
});


/**
 * SAVE SEARCH
 */
app.post("/save", function (req, res) {
  let data = new database.Search(req.body);
  data.save().then(item => {
      res.json(item);
  }).catch(err => {res.status(400).send("unable to save to database");});
});



/**
 * SEND EMAIL
 */
app.post("/sendEmail", function (req, res) {
  // Send Email
  sendEmail({from: 'umboweti.auto@gmail.com', to: req.body.email, subject: req.body.subject, html: req.body.content});
  res.send("Email enviado...");
});


// Start server on port 3000
app.listen(3000, function() {
  console.log("API Server listenting on port 3000");
})
