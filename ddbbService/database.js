const mongoose = require("mongoose");

// Define Database connection: We will be waiting for a services called mongo defined on docker compose file.
const DATABASE_CONNECTION = "mongodb://mongo/searchs";

let SearchSchema = mongoose.Schema({search:String, result: Array, total: Number});

Search = exports.Search = mongoose.model('Search', SearchSchema);

exports.initializeMongo = function()  {

  mongoose.connect(DATABASE_CONNECTION);
  console.log(`Trying to connecto to ${DATABASE_CONNECTION}`);

  let db = mongoose.connection;
  db.on("error", console.error.bind(console, "Not Connected to DB!"));

  db.once("open", function() {
    console.log("Connected to DDBB!");
    // addRandomSearch();
  })

}


//let addRandomSearch = () => {
//
//  let search = new Search({search: "hello world", results: []});
//
//  search.save( function(error, fluffy) {
//    if(error) console.log(error);
//    console.log("New search stored on DDBB!");
//  })
//
//
//}
