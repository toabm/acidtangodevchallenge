# ACID TANGO -- DEV CHALLENGE

> This is my proposed solution for the code challenge proposed by AcidTango that you can find on this repo under
> [AcidTango Challenge](AcidTangoCodeChallenge.pdf)

## 1 - SUM UP

The solution to the challenge has 2 different parts and makes use of the following technologies:

+ **MongoDB:** For persistance of text searchs.
+ **Server:** Programed with ExpressJS, making use of NodeJS ES6 modules and Mongoose library to connect with DB.
+ **SPA Web:** A Web Interface in AngularJS using Angular CLI to send requests to the server, get the corresponding response and save searchs and results.



## 2 - LET'S DOCKER

The whole solution is dockerized.

The server folder contains the docker-compose.yml needed to run the API server within a docker container. The server/www
dir contains a build of the front SPA that will be delivered by the same API server under http://localhost:3000/

Just download the corresponding docker image by:


```
    $ cd server
    $ docker push umboweti/acidtango_challenge:latest
```
Start the docker container by;
```
    $ docker-compose up
```

Now we have our server running:

+ The web SPA can be obtained under http://localhost:3000/
+ The API server path is http://localhost:3000/api/v1/ --> We will see "API Server working smoothly...:)" if we try this URL on our browser.

> Keep reading even if you already used docker to have our server UP for deeper understanding of each module.

## 2 - OR... NOT: INSTALLATION

### THE WEB SPA

The Web interface is un the folder [www](www)

The web interface consists on a simple SPA (single page application), programmed on JS, making use of AngularJS to provide
modules implementations, SCSS compilation, TS compilation, babel transpilation and some other nice features.

It is a NPM project and we will make use of npm scripts to deploy it.

First we navigate to the corresponding folder and install the dependencies.

```
    $ cd www
    $ npm i
```

After that we just have to make use of npm scripts to deploy the app under server/www/ folder.
```
    $ npm run build   // Will deploy the app for developement under server/www/ folder
```

### THE MONGO DB SERVICE

To start hte MongoDB service our server needs to store and obtain data we just execute the following command under root folder:
```
    $ docker-compose up
```

The docker-compose.yml file contained under root folder contains instructions to set up a MongoDB server on port 27017.



### THE API SERVER

The server here makes all the hard work. It is an API implemented on NodeJS using ExpressJS framework.
It is entirely coded on TS and it is another NPM project. In [package.json](server/package.json) we can see
the scripts we will use to deploy our server.

First we must install the corresponding dependencies

```
    $ cd server
    $ npm i
```

Then we can start the server with one of the following:
```
    $ npm start         // Will start API listening on port 3000
    $ npm run debug     // Will start API on port 3000 with debug info con console.
```

The server will listen to both request on:
+ [localhost:3000](http://localhost:3000/)               --> Will serve static files to allow us to see the web SPA.
+ [localhost:3000/api/v1/](http://localhost:3000/api/v1/)   --> Implements the API endpoints.

We have implemented 2 different APIs in our server, each one in a different module:
+ http://localhost:3000/api/v1/chucknorris/ endpoints will feed from chucknorris.io API to answer.
+ http://localhost:3000/api/v1/searchs/ endpoints will connect to with our MongoDB ddbb to save searchs made by user. It will also send the emails.

You can find also a POSTMAN COLLECTION to check the server API [here](server/AcidTangoCodeChallenge.postman_collection.json)

The server will serv static files to show the web SPA on server/www

If you open the web SPA on your browser (preferibly google chrome) setting the URL [localhost:3000](http://localhost:3000/),  you will see something like this:

![App ScreenShot](www/src/assets/images/sample_UI.png.png)



## 3 - TESTING THE APP

Mocha testing framework has been implemented for the server part but it is far from complete. We have 2 scripts on server/package.json
to run the tests:
```
    $ npm run test         // Will run tests without debug information: clean output.
    $ npm run test-debug   // Will run tests with a lot of debug information on coonsole.
```

So far there are only 1 tests implemented:
+ Test for GET searchs endpoint.



## 3 - ddbbService

Forget about folder ddbbService, it is just a simpler implementation of the server thought for the SPA to request
jokes directly to chucknorris.io API, and implements just the searchs part of the Server in a very simple way but:

- No tests
- No use of typescript.
- Not modular.
- No restfull.




