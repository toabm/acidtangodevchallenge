import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JokeByCategoryComponent } from './joke-by-category.component';

describe('JokeByCategoryComponent', () => {
  let component: JokeByCategoryComponent;
  let fixture: ComponentFixture<JokeByCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JokeByCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JokeByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
