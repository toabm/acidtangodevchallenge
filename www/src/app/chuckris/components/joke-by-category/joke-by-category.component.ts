import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../services/api.service";
import {Chascarrillo} from "../../models/chascarrillo";

@Component({
  selector: 'app-joke-by-category',
  templateUrl: './joke-by-category.component.html',
  styleUrls: ['./joke-by-category.component.scss']
})

export class JokeByCategoryComponent implements OnInit {

  // List of possible categories.
  public categories: String[] = [];
  // Selected category on UI
  public selectedCategory:String = '';
  // Our Chascarrillo instance to store the jokes.
  public categoryJoke: Chascarrillo = { value: "", icon_url: "", id: "", url: "",  categories: [] };

  /**
   * Class Constructor: Bind ApiService
   * @param apiService
   */
  constructor(private apiService: ApiService) { }

  /**
   * OnInit: Get a list of categories from API to feed UI.
   */
  ngOnInit(): void {
    this.apiService.getCategories().subscribe(categoriesList => (this.categories = categoriesList));
  }

  /**
   * On selected category changed, retrieve a new Joke from API.
   *
   * @param newValue
   */
  onChange(newValue:String) {
    this.selectedCategory = newValue;
    // Get random joke by category and update UI·
    this.apiService.getChascarrilloByCategory(this.selectedCategory)
      .subscribe(categoryJoke => this.categoryJoke = categoryJoke);
  }

}
