import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../services/api.service";
import {Chascarrillo} from "../../models/chascarrillo";

@Component({
  selector: 'app-joke-search',
  templateUrl: './joke-search.component.html',
  styleUrls: ['./joke-search.component.scss']
})
export class JokeSearchComponent implements OnInit {

  // Var to store the search results.
  public searchJokes: { total: number; result: Chascarrillo[]; }  = {total: 0, result: []};
  // Pagination parameters.
  public pageNumber: number = 1;
  public itemsPerPAge: number = 10;
  // Bool to know if user wants search results to be sent by email
  public sendEmail = false;
  // Var to store the email destination.
  public email: string = '';


  /**
   * Class constructor
   *
   * @param apiService Bind ApiService
   */
  constructor(private apiService: ApiService) { }
  ngOnInit(): void {}

  /**
   * On text input event we will call API to perform corresponding search.
   *
   * Will unpate UI feeding results and will save the results on our Searchs API.
   *
   * If the corresponding checkbox is selected, results will be sent to indicated email.
   * @param event
   */
  onChangeText(event: any){
    // Get random joke by category and update UI·
    if(event.target.value == '') return;
    this.apiService.getChascarrilloByText(event.target.value)
      .subscribe(searchJokes => {
        this.searchJokes = searchJokes;
        this.saveSearch(searchJokes, event.target.value); // Store results on out Searchs API.
        if (this.validateEmail(this.email) && this.sendEmail) this.apiService.sendEmail(this.email, this.searchResultToHTMLTable(searchJokes.result),
          `Found ${searchJokes.total} facts searching "${searchJokes.search}"` ).subscribe( result => {console.log("EMAIL SENT!")});
        this.pageNumber = 1; // Set paginator on first page after changing table contents.
      });
  }

  /**
   * This method will use ApiService to store search results from apiService.apiUrl API to apiService.searchsUrl API.
   *
   * @param search
   * @param text
   */
  saveSearch(search: { search: String; } , text: String) {
    search.search = text;
    this.apiService.saveNewSearch(search).subscribe( result => {console.log("SEARCH SAVED ON DDBB!")})
  }


  /**
   * Returns true if mail is a well formatted email string.
   *
   * @param mail
   */
  validateEmail(mail: string) {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail);
  }


  /**
   *
   * @param result
   */
  searchResultToHTMLTable(result: any[]) {
    return `
        <!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head><style>
                tbody tr:nth-child(2n-1), tbody tr:nth-child(2n-1) td { background-color: lightcyan;}
                td.icon { display: table-cell; vertical-align: middle; }
                td img { max-width: 2rem;}
        </style></head>
        <body><table>
            <thead> <tr><th>Id</th><th>Joke</th><th>URL</th><th>Categories</th><th>Icon</th></tr></thead>
            <tbody>
                ${result.map( joke => `<tr>
                      <td>${joke.id}</td>
                      <td><strong>${joke.value}</strong></td>
                      <td><a href="{{joke.icon_url}}">${joke.url}</a></td>
                      <td>${joke.categories}</td>
                      <td class="icon"><img src="${joke.icon_url}"></td>
                    </tr>`).join("")}
            </tbody>
      </table></body></html>`
  }

}
