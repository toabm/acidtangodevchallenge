import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.scss']
})
export class JokeComponent implements OnInit {

  /**
   * Class constructor
   *
   * @param apiService Bind ApiService
   */
  constructor(private apiService: ApiService, private router: Router) { }

  /**
   *
   */
  ngOnInit(): void {}


  /**
   * Use AppRoutingModule to navigate to '/album-add' component.
   */
  goTo(section:String): void {this.router.navigate([section])}
}
