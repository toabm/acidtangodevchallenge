import { Component, OnInit } from '@angular/core';
import {Chascarrillo} from "../../models/chascarrillo";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-joke-random',
  templateUrl: './joke-random.component.html',
  styleUrls: ['./joke-random.component.scss']
})
export class JokeRandomComponent implements OnInit {

  // Our Chascarrillo instance to store the jokes.
  public randomJoke: Chascarrillo = { value: "", icon_url: "", id: "", url: "", categories: [] };

  /**
   * Class Constructor: Bind ApiService
   * @param apiService
   */
  constructor(private apiService: ApiService) { }

  /**
   * OnInit: Get a random joke from API to feed UI.
   */
  ngOnInit(): void {
    // Show random joke at startup.
    this.getRandomJoke();
  }

  /**
   * This method retrieves a random chucknorris joke from API server.
   */
  getRandomJoke = () => this.apiService.getRandomChascarrillo().subscribe(joke => (this.randomJoke = joke));

}
