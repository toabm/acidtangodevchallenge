import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {JokeComponent } from './components/joke/joke.component'
import {FormsModule} from "@angular/forms";
import { JokeByCategoryComponent } from './components/joke-by-category/joke-by-category.component';
import { JokeSearchComponent } from './components/joke-search/joke-search.component';
import {NavBarComponent} from "./components/nav-bar/nav-bar.component";
import {NavDrawerComponent} from "./components/nav-bar/nav-drawer/nav-drawer.component";
import {RouterModule} from "@angular/router";
import { JokeRandomComponent } from './components/joke-random/joke-random.component';
import { NgxPaginationModule } from 'ngx-pagination'; // Importing the pagination module for the application.

@NgModule({
  declarations: [ JokeComponent, JokeByCategoryComponent, JokeSearchComponent, NavBarComponent, NavDrawerComponent, JokeRandomComponent],
  imports: [CommonModule, FormsModule, RouterModule, NgxPaginationModule],
  exports: [ NavBarComponent]
})
export class ChuckrisModule implements OnInit {
  constructor() {}
  ngOnInit() {}
}
