import {OnInit} from "@angular/core";

export interface Chascarrillo {
  value: string; // The joke itself
  icon_url: string;
  id: string; // Joke ID
  url: string; // URL to get the joke
  categories: []; // Array of categories the joke belongs to
}

export class Chascarrillo implements Chascarrillo {

  constructor() {
  }
}
