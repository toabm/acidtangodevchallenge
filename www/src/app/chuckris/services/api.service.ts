/**
 * This service will centralize all chucknorris.io API server calls.
 */
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

// Import our Chascarrillo model
import {Chascarrillo} from  '../models/chascarrillo'

@Injectable({providedIn: 'root'})
export class ApiService {

  // Our base API URL
  private chuckNorrisApiURL = "http://localhost:3000/api/v1/chucknorris";
  private searchsApiURL     = "http://localhost:3000/api/v1/searchs";

  /**
   * Class constructor.
   *
   * @param http Bind HttpClient for http requests.
   */
  constructor(private http: HttpClient) {}


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // CHUCKNORRIS API ///////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   * This method gets list of chucknorris API categories.
   */
  public getCategories(): Observable<String[]> {
    return this.http.get<[]>(`${this.chuckNorrisApiURL}/categories`);
  }

  /**
   * This method will request server for a Chascarrillo.
   */
  public getRandomChascarrillo(): Observable<Chascarrillo> {
    return this.http.get<Chascarrillo>(`${this.chuckNorrisApiURL}/random`);
  }

  /**
   * Get random joke by category
   * @param category
   */
  getChascarrilloByCategory(category: String): Observable<Chascarrillo> {
    return this.http.get<Chascarrillo>(`${this.chuckNorrisApiURL}/random?category=${category}`);
  }

  /**
   * Get joke by free text search.
   * @param text
   */
  getChascarrilloByText(text: String): Observable<any> {
    return this.http.get<any>(`${this.chuckNorrisApiURL}/search?query=${text}`);
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // SAVE SEARCHS API //////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   * @param search
   */
  saveNewSearch(search: {}): Observable<any> {
    return this.http.post<any>(`${this.searchsApiURL}`, search);
  }


  /**
   *
   * @param email
   * @param content
   */
  sendEmail( email: String, content: {}, subject: string){
    return this.http.post<any>(`${this.searchsApiURL}/email`, { email: email, content: content, subject: subject});
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
