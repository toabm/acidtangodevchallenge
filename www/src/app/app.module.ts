// https://github.com/jjcermeno/chuck-norris-jjcermeno

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";

// Import other modules from root AppModule
import { ChuckrisModule } from "./chuckris/chuckris.module";


// Import Compoents
import { AppComponent } from './app.component';


@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, ChuckrisModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
